﻿open System
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.Hosting

// quick sort
let rec q =
    function
    | [] -> []
    | f :: r ->
        let s, l = List.partition ((>=) f) r
        List.concat [ q s; [ f ]; q l ]

// lotto number generator
let lottoNumbers minValue maxValue count =
    let f = fun _ -> Random(int DateTime.Now.Ticks).Next(minValue, maxValue + 1)
    Seq.initInfinite f |> Seq.distinct |> Seq.take count

//  book types
type Author = { name: string }

type Book =
    { name: string
      pages: int
      published: DateTime
      author: Author }


[<EntryPoint>]
let main args =
    let builder = WebApplication.CreateBuilder(args)
    let app = builder.Build()

    app.MapGet("/", Func<string>(fun () -> "Hello World!")) |> ignore
    app.MapGet("/list", Func<seq<int>>(fun () -> lottoNumbers 1 49 6)) |> ignore
    app.MapGet("/quicksort", Func<seq<int>>(fun () -> q [ 4; 3; 22; 1 ])) |> ignore

    app.MapPost("/quicksort2", Func<seq<int>>(fun () -> q [ 4; 3; 22; 1 ]))
    |> ignore

    app.MapGet("/hello", Func<string[]>(fun () -> [| "Hello"; "World"; "First F#/ASP.NET Core web API!" |]))
    |> ignore

    app.MapGet(
        "/dict",
        Func<Book>(fun () ->
            { name = "Expert F#"
              pages = 1
              published = DateTime.Now
              author = { name = "uskusi" } })
    )
    |> ignore

    app.Run()
    0
